package com.luisgaviria.callcenter.exceptions;

public class NotAvailableEmployeeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotAvailableEmployeeException(String message) {
        super(message);
    }
}
